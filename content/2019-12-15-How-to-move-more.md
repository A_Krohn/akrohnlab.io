+++
title = "Wie kann ich mich mehr bewegen?"
description = "Lerne wie man sich mehr bewegt"
[taxonomies]
tags = ["move", "health", "Customer", "ID-3", "AKrohn", "DONE"]
categories = ["user_story", "acceptance_criteria", ]
+++

# User Story
Als Kunde möchte ich lernen wie ich mich mehr bewegen kann, sodass ich länger Leben kann.

# Acceptance Criteria
Der Abschnitt zum Lernen ist abgeschlossen, wenn ein Nutzer 
* eine Umfrage zur Bewegung im Alltag beantwortet
  * Wurde abgeschlossen durch die Erstellung dieser Webseite [here](https://spitto.eu/how-to-move-more/) und der dort enthaltendenUmfrage

# Importance
Als Unternehmer lerne ich anhand vom Fragebogen wie sich Menschen mehr bewegen wollen, sodass ich Bussinessmodelle als Unterstützung für den Kunden anbieten kann 

# Estimate:

5 Tage

* Abgeschlossen am 20.12.2019 in 5 Tagen
* SUCCESS
