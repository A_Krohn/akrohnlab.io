+++
title = "Zeige gps data from public move actions"
description = "Show it in six angle raater"
[taxonomies]
tags = ["data", "Customer", "ID-5", "AKrohn", "OPEN"]
categories = ["user_story", "acceptance_criteria", ]
+++

# User Story
Als Kunde möchte ich meine gps daten in einer Karte sehen und erkennen wo ich in der Vergangenheit war

# Acceptance Criteria
GPS daten von einer öffentlichen Person können in einer Webseite untersucht werden
* Orte die die Person in der Vergangenheit besucht hat
* Orte die noch nicht besucht worden sind
* Aufenthaltadauer einzelner Orte

# Importance
Als Unternehmer lerne ich welche Features den Kunden am meisten interessieren

# Estimate:

10 Tage

