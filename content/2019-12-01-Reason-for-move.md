+++
title = "Reason for movement"
description = "Learn which reason exist for movement"
[taxonomies]
tags = ["move", "health", "Customer", "ID-1", "AKrohn", "DONE"]
categories = ["user_story", "acceptance_criteria", ]
+++

# User Story

As Customer I learn why movement is important so that I know why I shall move as much as possible

# Acceptance Criteria

* The learning section to movement is completed when I can mention ten reason why I shall move as much as possible
  * Have been fulfilled with the survey link in the report [here](https://spitto.eu/reason-to-move/)

# Importance

Business use:
* 2

Customer use:
* 3

# Estimate:
* 5 

# Required
* 3 days, finished at 03.12.2019

