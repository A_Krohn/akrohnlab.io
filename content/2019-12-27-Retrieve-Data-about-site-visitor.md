+++
title = "Erhalte Informationen über die Webseitenbesucher?"
description = "Dauer, Inhalt usw."
[taxonomies]
tags = ["data", "Business", "ID-4", "AKrohn", "OPEN"]
categories = ["user_story", "acceptance_criteria", ]
+++

# User Story
Als Unternehmer möchte ich lernen welche Abschnitte auf der spitto.eu Webseite den Besucher am besten gefallen.

# Acceptance Criteria
Folgende Parameter werden von einer Tracking Software erfasst:
* Dauer, Häufigkeit eines Webseitenbesucher
* Webseitenabschnitte die von einen Webseitenbesucher bevorzugt besucht werden

# Importance
Als Unternehmer lerne ich anhand der Trackingsoftware, was Kinden interessiert

# Estimate:

5 Tage

