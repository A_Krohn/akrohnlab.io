+++
title = "Datenschutzerklärung"
description = "Verstehe was in einer Datenschutzeeklärung enthalten sein muss"
[taxonomies]
tags = ["Datenschutz", "Customer", "ID-2", "AKrohn", "ONGOING"]
categories = ["user_story", "acceptance_criteria", ]
+++

# User Story

Als Kunde verstehe ich die Datenschutzerklärung sodass ich innerhalb von 2 Minuten meine Rechte umsetzen kann.

# Acceptance Criteria

* Die Datenschutzerklärung ist akzeptiert, wenn 2 Personen diese nachvollziehbar finden

# Importance

Business use:
* 2

Customer use:
* 3

# Estimate:
* 3
